require('./boot')

import Vue from 'vue'
import store from './store'
import VueRouter from 'vue-router'
import routes from './router';
import mixin from './mixin'


import App from './App.vue'
Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes
});

Vue.config.productionTip = false
new Vue({
  render: h => h(App),
  store: store,
  router
}).$mount('#app')
Vue.mixin(mixin)