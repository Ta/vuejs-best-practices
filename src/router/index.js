

const routes = [
  {
    path: '/',
    component: () => import('../layouts/MainLayout.vue'),
    meta: {
      member: true
    },
    children: [
      {
        path: 'top',
        component: () => import('../pages/Top.vue'),
        name: 'top',
        meta: {
          auth: true,
          top:true,
          title: 'task.title'
        }
      },
      {
        path: 'profile',
        component: () => import('../pages/Profile.vue'),
        name: 'profile',
        meta: {
          auth: true,
          top:true,
          title: 'UserProfile'
        }
      }
    ]
  },
  {
    path: '/admin',
    component: () => import('../layouts/AdminLayout.vue'),
    meta: {
      auth: true,
      owner: true
    },
    children: [
      {
        path: 'user',
        component: () => import('../pages/admin/user/UserList.vue'),
        name: 'Organization Information',
        meta: {
          auth: true,
          owner: true,
          title: 'organization.mOrganizationInfo',
          parent: '/setting',
          parentPC: '/orgadmin',
        }
      }
    ]
  }
]


export default routes
