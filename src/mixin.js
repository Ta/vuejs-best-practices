var mixin = {
    data: function () {
      return {
        message: 'hello',
        foo: 'abc'
      }
    },

    methods: {
        getPageTitle(){
            return this.getLastRoute.meta.title
        }
    },

    computed: {
        getLastRoute(){
            const matchedRouters = this.$route.matched
            return matchedRouters[matchedRouters.length -1]
        }
    }
}

export default mixin
  